# 网约车地图-设计思路

## 步骤

### 平移思路

1. 我们的gps数据是车辆5秒上报一次位置数据；前端请求数据获取当前最新的gps位置； 
2. 第一次请求到数据后我把所有的车辆先标记到地图上； 
3. 标记到地图上以后，需要把marker存放到list记录下来(为了更新移动使用); 
4. 当再次请求到新的gps数据的时候，我们需要平滑的移动到最新的位置，`关键问题是再这里；怎么样在才能平滑移动呢`，无非就是需要不断的移动从当前位置到最新的位置，这里就当时从A点移动到B点，无非就是marker.setPosition(),不断的调用知道B点;

### 路线规划思路
- 百度地图V3接口
1. 规划一条线路，起点终点。
2. 当行驶点发生偏移-到一定距离时候-重新规划路线。
3. 计算已经走过的路线并渲染到页面
- 百度地图V2接口
1. 规划一条线路，起点终点
2. 当行驶点发生偏移到一定距离之后-重新规划路线，将当前点设置为必经点，路线则自动规划。
- 高德地图